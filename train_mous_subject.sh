#!/usr/bin/env bash
# Trains a DiffE model using a subject from the MOUS dataset.
#
# Usage:
#   $ ./train_mous_subject.sh [SUBJECT] [NUM_CLASSES] [DATA_PATH]
#
# Subject must be in XYYYY format, for example A2002.

SUBJECT="${1:-A2002}"
NUM_CLASSES="${2:-2}"
DATA_PATH="${3:-../datasets/MOUS.diffe/}"

LOGFILE="logs/train_mous_s${SUBJECT}_t${NUM_CLASSES}.log"

DIR="$(dirname "$(readlink -f "${0}")")"
pushd "${DIR}" || exit 255
mkdir -p "$(dirname "${LOGFILE}")"

time python main.py \
  --data_path "${DATA_PATH}/" \
  --extension pkl \
  --list_subjects "${SUBJECT}" \
  --num_classes "${NUM_CLASSES}" 2>&1 \
  | tee "train_mous_s${SUBJECT}_t${NUM_CLASSES}.log"

popd || exit 255
