#!/usr/bin/env bash
# Trains a DiffE model for each of the subjects in the MOUS dataset.
#
# Usage:
#   $ ./train_mous_all.sh [DATA_PATH] [NUM_CLASSES]

# The MOUS dataset path formated for train the DiffE model.
DATA_PATH="${1:-../datasets/MOUS.diffe}"
NUM_CLASSES="${2:-2}"

list_subjects() {
  # Searchs for subject names from the MOUS dataset formated for DiffE train.

  local DATA_PATH="${1}"

  \ls "${DATA_PATH}"/s*_sess*.pkl | sed -E 's@^.*/s(.*)_sess.*\.pkl$@\1@'
}

DIR="$(dirname "$(readlink -f "${0}")")"
pushd "${DIR}" || exit 255

for SUBJECT in $(list_subjects "${DATA_PATH}")
do
  [ -e models/diffe_${SUBJECT}.pt ] && continue  # skip, already trained.
  echo "Subject: ${SUBJECT}"
  ./train_mous_subject.sh "${SUBJECT}" "${NUM_CLASSES}" "${DATA_PATH}"
done

popd || exit 255
