#!/usr/bin/env bash
# Trains a DiffE model using a subject from the MEG-MASC dataset.
#
# Usage:
#   $ ./train_mous_subject.sh [SUBJECTS] [NUM_CLASSES] [DATA_PATH]
#
# Subject must be in XX format, for example 01.

SUBJECTS="${1:-01 02 04 05}"
NUM_CLASSES="${2:-2}"
DATA_PATH="${3:-../datasets/MEG-MASC.phonation/}"

LOGFILE="logs/train_mous_s${SUBJECTS// /-}_t${NUM_CLASSES}.log"

DIR="$(dirname "$(readlink -f "${0}")")"
pushd "${DIR}" || exit 255
mkdir -p "$(dirname "${LOGFILE}")"

time python main.py \
  --data_path "${DATA_PATH}/" \
  --extension pkl \
  --list_subjects ${SUBJECTS} \
  --list_sessions 0 1 \
  --num_classes "${NUM_CLASSES}" \
  "${@}" \
  2>&1 | tee "train_meg_masc_s${SUBJECTS// /-}_t${NUM_CLASSES}.log"

popd || exit 255
